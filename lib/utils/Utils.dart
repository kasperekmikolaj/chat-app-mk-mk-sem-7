import 'dart:convert';

import 'package:uranos/models/User.dart';

import '../constants.dart';
import '../sign_in.dart';

class Utils {
  static getChatId({firstUserId: String, secondUserId: String}) {
    int compareToResult = firstUserId.compareTo(secondUserId);

    String chatId = compareToResult < 0
        ? firstUserId + secondUserId
        : secondUserId + firstUserId;

    return chatId;
  }

  static Future<User> getCurrentUserFutureFromSecureStorage() async {
    String currentUserJson = await secureStorage.read(key: kCurrentUserKey);
    return User.fromJsonSecureStorage(jsonDecode(currentUserJson));
  }
}
