import 'package:flutter/material.dart';
import 'package:uranos/constants.dart';
import 'package:uranos/models/User.dart';
import 'package:uranos/widgets/LoadingContainer.dart';
import 'package:uranos/widgets/buttons/PurpleCircleButton.dart';
import 'package:uranos/widgets/UserInfoContainer.dart';

class ProfileInfoScreen extends StatelessWidget {
  final String appBarTitle;
  final Future<User> userFuture;
  final PurpleCircleButton button;

  ProfileInfoScreen(this.appBarTitle, this.userFuture, this.button);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle),
      ),
      body: Container(
        decoration: kInfoScreenBoxDecoration,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FutureBuilder(
                  future: userFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      return UserInfoColumn(
                        button: button,
                        user: snapshot.data,
                      );
                    } else {
                      return LoadingScreen();
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
