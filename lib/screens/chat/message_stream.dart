import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uranos/screens/chat/chat_screen.dart';
import 'package:uranos/screens/chat/message_widgets/message_base_class.dart';
import 'package:uranos/screens/chat/message_widgets/message_image.dart';
import 'package:uranos/screens/chat/message_widgets/message_music.dart';
import 'package:uranos/screens/chat/message_widgets/message_video.dart';
import '../../constants.dart';
import 'chat_screen.dart';
import 'message_widgets/message_bubble.dart';

class MessagesStream extends StatelessWidget {
  final String chatId;
  final String currentUserId;
  final Set<MessageBase> messageBubblesCache = {};

  MessagesStream({Key key, @required this.chatId, @required this.currentUserId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: firestore
          .collection('chatRooms')
          .document(chatId)
          .collection('messages')
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.lightBlueAccent,
            ),
          );
        }
        final messages = snapshot.data.documents.reversed;
        List<MessageBase> messageBubbles = [];
        messageBubbles.addAll(messageBubblesCache);
        for (var message in messages) {
          final messageId = message.documentID;
          final messageText = message.data['text'];
          final messageSenderId = message.data['senderId'];
          final messageSenderDisplayName = message.data['senderDisplayName'];
          final timestamp = message.data['time'];

          if(messageBubbles.where((element) => element.id == messageId).isEmpty){
            MessageBase bubble;
            switch(message.data['type']) {
              case ChatScreen.kMessageType: {
                bubble = MessageBubble(
                  id: messageId,
                  senderDisplayName: messageSenderDisplayName,
                  text: messageText,
                  isMe: currentUserId == messageSenderId,
                  time: timestamp,
                );
              }
              break;

              case ChatScreen.kImageType: {
                bubble = MessageImage(
                  id: message.documentID,
                  senderDisplayName: messageSenderDisplayName,
                  isMe: currentUserId == messageSenderId,
                  time: timestamp,
                  imageUrl: messageText,
                );
              }
              break;

              case ChatScreen.kVideoType: {
                bubble = MessageVideo(
                  id: message.documentID,
                  senderDisplayName: messageSenderDisplayName,
                  isMe: currentUserId == messageSenderId,
                  time: timestamp,
                  videoUrl: messageText,
                );
              }
              break;

              case ChatScreen.kMusicType: {
                bubble = MessageMusic(
                  id: messageId,
                  musicUrl: messageText,
                  senderDisplayName: messageSenderDisplayName,
                  isMe: currentUserId == messageSenderId,
                  time: timestamp,);
              }
              break;
            }
            messageBubblesCache.add(bubble);
            messageBubbles.add(bubble);
          }
        } // for message
        messageBubbles.sort((a, b) => b.time.compareTo(a.time));
        return Expanded(
          child: ListView(
            addAutomaticKeepAlives: false,
            reverse: true,
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: messageBubbles,
          ),
        );
      },
    );
  }
}
