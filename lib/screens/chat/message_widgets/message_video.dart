import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'message_base_class.dart';

class MessageVideo extends MessageBase {
  final String videoUrl;

  MessageVideo(
      {@required id,
      @required time,
      @required this.videoUrl,
      @required isMe,
      @required senderDisplayName})
      : super(
            id: id,
            time: time,
            isMe: isMe,
            senderDisplayName: senderDisplayName);

  @override
  State<StatefulWidget> createState() => _MessageVideo();
}

class _MessageVideo extends State<MessageVideo> {
  ChewieController _chewieController;
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();

    _controller = VideoPlayerController.network(
      widget.videoUrl,
    );
    _controller.setLooping(true);

    _chewieController = ChewieController(
        videoPlayerController: _controller,
        aspectRatio: _controller.value.aspectRatio,
        autoInitialize: true,
        looping: true,
        errorBuilder: (context, errorMessage) {
          return Center(
            child: Text(
              errorMessage,
              style: TextStyle(color: Colors.white),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(
          widget.isMe ? 100 : 0, 0, widget.isMe ? 0 : 100, 0),
      child: Column(
        crossAxisAlignment:
            widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.senderDisplayName,
            style: TextStyle(
              fontSize: 10.0,
              color: Colors.white,
            ),
          ),
          Container(
            child: Chewie(
              controller: _chewieController,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();

    _controller.dispose();
    _chewieController.dispose();
  }
}
