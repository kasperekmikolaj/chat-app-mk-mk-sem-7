import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

abstract class MessageBase extends StatefulWidget {
  final String id;
  final Timestamp time;
  final bool isMe;
  final String senderDisplayName;

  MessageBase(
      {@required this.id,
        @required this.time,
      @required this.isMe,
      @required this.senderDisplayName});

  @override
  bool operator ==(Object other) =>
              other is MessageBase &&
              id == other.id;

  @override
  int get hashCode =>
      super.hashCode ^
      id.hashCode;

}
