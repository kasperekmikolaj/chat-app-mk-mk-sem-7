import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uranos/screens/chat/message_widgets/message_base_class.dart';

class MessageMusic extends MessageBase {

  final String musicUrl;

  MessageMusic({id, this.musicUrl, senderDisplayName, isMe, time})
      : super(id: id, time: time, isMe: isMe, senderDisplayName: senderDisplayName);

  @override
  _MessageMusicState createState() => _MessageMusicState();
}

class _MessageMusicState extends State<MessageMusic> {
  AudioPlayer _audioPlayer = AudioPlayer();
  bool isPlaying = false;
  String currentTime = "00:00";
  int currentTimeMilliSeconds = 0;
  int maxTimeMilliSeconds = 0;

  @override
  void initState() {
    super.initState();

    _audioPlayer.onAudioPositionChanged.listen((Duration duration) {
      setState(() {
        currentTime = duration.toString().split('.')[0].substring(2);
        currentTimeMilliSeconds = duration.inMilliseconds;
      });
    });

    _audioPlayer.onDurationChanged.listen((Duration duration) {
      setState(() {
        maxTimeMilliSeconds = duration.inMilliseconds;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(widget.isMe ? 80 : 0, 0, widget.isMe ? 0 : 80, 0),
      child: Column(
        crossAxisAlignment:
        widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            widget.senderDisplayName,
            style: TextStyle(
              fontSize: 10.0,
              color: Colors.white,
            ),
          ),
          Material(
            borderRadius: widget.isMe
                ? BorderRadius.only(
                topLeft: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0))
                : BorderRadius.only(
              bottomLeft: Radius.circular(30.0),
              bottomRight: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
            elevation: 5.0,
            color: widget.isMe ? Colors.lightBlueAccent : Colors.white38,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    isPlaying ? Icons.pause : Icons.play_arrow,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () async {
                    int status;
                    if(!isPlaying){
                      status = await _audioPlayer.play(widget.musicUrl, isLocal: false);
                      if(status == 1){
                        setState(() {
                          isPlaying = true;
                        });
                      }
                    }
                    else {
                      int status = await _audioPlayer.pause();
                      if(status == 1){
                        setState(() {
                          isPlaying = false;
                        });
                      }
                    }
                  },
                ),
                Slider(
                  onChanged: (double position) {
                    _audioPlayer.seek(Duration(milliseconds: position.toInt()));
                    currentTimeMilliSeconds = position.toInt();
                  },
                  value: currentTimeMilliSeconds.toDouble(),
                  min: 0,
                  max: maxTimeMilliSeconds.toDouble(),
                  activeColor: widget.isMe ? Colors.white70 : Colors.cyan,
                ),
                Text(currentTime),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
