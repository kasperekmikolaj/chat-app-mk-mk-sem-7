import 'package:flutter/material.dart';
import 'package:uranos/screens/chat/message_widgets/message_base_class.dart';

class MessageBubble extends MessageBase {

  final String text;

  MessageBubble({id, senderDisplayName, this.text, isMe, time})
      : super(id: id, time: time, isMe: isMe, senderDisplayName: senderDisplayName);

  @override
  State<StatefulWidget> createState() => _MessageBubble();
}

class _MessageBubble extends State<MessageBubble>{
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(widget.isMe ? 60 : 0, 0, widget.isMe ? 0 : 60, 0),
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment:
          widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.senderDisplayName,
              style: TextStyle(
                fontSize: 10.0,
                color: Colors.white,
              ),
            ),
            Material(
              borderRadius: widget.isMe
                  ? BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  bottomLeft: Radius.circular(30.0),
                  bottomRight: Radius.circular(30.0))
                  : BorderRadius.only(
                bottomLeft: Radius.circular(30.0),
                bottomRight: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
              elevation: 5.0,
              color: widget.isMe ? Colors.lightBlueAccent : Colors.white,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                child: Text(
                  widget.text,
                  style: TextStyle(
                    color: widget.isMe ? Colors.white : Colors.black,
                    fontSize: 15.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
