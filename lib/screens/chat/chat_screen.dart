import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:uranos/constants.dart';
import 'package:uranos/models/User.dart';
import 'package:uranos/utils/Utils.dart';
import 'package:uranos/widgets/LoadingContainer.dart';
import 'package:uranos/widgets/background_image.dart';

import 'message_stream.dart';

class ChatScreen extends StatefulWidget {
  final String currentUserId;
  final String friendId;
  final String chatId;

  static const String kImageType = 'ImageType';
  static const String kMessageType = 'TextType';
  static const String kVideoType = 'VideoType';
  static const String kMusicType = 'MusicType';

  ChatScreen(
      {Key key,
      @required this.currentUserId,
      @required this.friendId,
      @required this.chatId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChatScreen();
}

class _ChatScreen extends State<ChatScreen> {
  final messageTextController = TextEditingController();

  String messageText = '';
  User currentUser;
  User friend;

  bool showLoadingScreen;
  String fileUrl;
  final List<String> listOfImageExtensions = ['jpg', 'jpeg', 'png'];
  final List<String> listOfVideoExtensions = ['mp4'];
  final List<String> listOfMusicExtensions = ['mp3'];

  @override
  void initState() {
    super.initState();
    showLoadingScreen = false;
    fileUrl = '';
    getUsers();
  }

  void getUsers() async {
    this.currentUser = await Utils.getCurrentUserFutureFromSecureStorage();

    DocumentSnapshot friendSnap =
        await firestore.collection('users').document(widget.friendId).get();

    setState(() {
      this.friend = User.fromJsonFirebase(
          json: friendSnap.data, userId: friendSnap.documentID);
    });
  }

  @override
  Widget build(BuildContext context) {
    return BackgroundImage(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: _createAppBar(),
        body: showLoadingScreen
            ? LoadingScreen()
            : SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    MessagesStream(
                      chatId: widget.chatId,
                      currentUserId: widget.currentUserId,
                    ),
                    Container(
                      decoration: kMessageContainerDecoration,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: IconButton(
                              icon: Icon(
                                Icons.image,
                                color: Colors.white,
                                size: 35,
                              ),
                              onPressed: () {
                                _onFileButtonClicked();
                              },
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.all(2.0),
                              child: TextField(
                                style: TextStyle(
                                  color: Colors.white
                                ),
                                controller: messageTextController,
                                onChanged: (value) {
                                  messageText = value;
                                },
                                decoration: kMessageTextFieldDecoration(
                                    hintText: 'Type your message here...'),
                              ),
                            ),
                          ),
                          FlatButton(
                            onPressed: () {
                              _sendTextMessage();
                            },
                            child: Text(
                              'Send',
                              style: kSendButtonTextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  AppBar _createAppBar() {
    if (friend != null) {
      return AppBar(
        title: Text(friend.displayName),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 1, 25, 1),
            child: CircleAvatar(
              radius: 28,
              backgroundImage: NetworkImage(friend.photoUrl),
            ),
          ),
        ],
      );
    } else {
      return AppBar(
        title: Text('Loading...'),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 1, 25, 1),
            child: CircleAvatar(
              radius: 28,
              backgroundImage: AssetImage('assets/uranos.png'),
            ),
          ),
        ],
      );
    }
  }

  Future<void> _onFileButtonClicked() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if (result != null) {
      setState(() {
        showLoadingScreen = true;
      });
      File file = File(result.files.single.path);
      String extension = result.files.single.extension;
      String type;
      if(listOfImageExtensions.contains(extension)){
        type = ChatScreen.kImageType;
      }
      else if(listOfVideoExtensions.contains(extension)){
        type = ChatScreen.kVideoType;
      }
      else if(listOfMusicExtensions.contains(extension)){
        type = ChatScreen.kMusicType;
      }
      _uploadFile(file, type);
    }
  }

  void _sendTextMessage() {
    if (messageText != '') {
      _sendToFirebase(messageText, ChatScreen.kMessageType);
      messageTextController.clear();
      messageText = '';
    } else {
      Fluttertoast.showToast(
        msg: 'Nothing to send!',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        fontSize: 18.0,
        textColor: Colors.black,
        backgroundColor: Colors.white,
      );
    }
  }

  Future _uploadFile(File uploadFile, String type) async {
    String fileName =
        DateTime.now().millisecondsSinceEpoch.toString() + currentUser.userId;
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = storageReference.putFile(uploadFile);
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
      fileUrl = downloadUrl;
      setState(() {
        showLoadingScreen = false;
        if (fileUrl != '') {
          _sendToFirebase(fileUrl, type);
          fileUrl = '';
        } else {
          Fluttertoast.showToast(
            msg: 'Image/Video sending error :(',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            fontSize: 18.0,
            textColor: Colors.black,
            backgroundColor: Colors.white,
          );
        }
      });
    }, onError: (err) {
      setState(() {
        showLoadingScreen = false;
      });
      Fluttertoast.showToast(msg: 'This file is not correct file');
    });
  }

  void _sendToFirebase(String text, String type) {
    firestore
        .collection('chatRooms')
        .document(widget.chatId)
        .collection('messages')
        .add({
      'text': text,
      'type': type,
      'senderId': currentUser.userId,
      'senderDisplayName': currentUser.displayName,
      'senderPhotoUrl': currentUser.photoUrl,
      'time': Timestamp.now(),
    });
  }
}
