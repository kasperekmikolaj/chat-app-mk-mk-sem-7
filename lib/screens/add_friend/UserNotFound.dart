import 'package:flutter/material.dart';

import '../../widgets/buttons/PurpleCircleButton.dart';

class UserNotFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            "User not found",
            style: TextStyle(
              fontSize: 25,
              color: Colors.black54,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        PurpleCircleButton(
          text: 'Ok',
          onPressedFunction: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
