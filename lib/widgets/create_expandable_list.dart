import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uranos/widgets/create_list_item.dart';

Widget createList(listOfDocuments, currentUserId, String header) {
  return Container(
    child: Card(
        color: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  header,
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            ListView.builder(
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount:
                  listOfDocuments.length == 0 ? 1 : listOfDocuments.length,
              itemBuilder: (context, index) {
                if (listOfDocuments.length == 0) {
                  String title;
                  if (header == 'Invitations') {
                    title = "No one invited you so far";
                  } else {
                    title = "Add friends to chat with them!";
                  }
                  return Padding(
                    padding: EdgeInsets.fromLTRB(0, 200, 0, 0),
                    child: ListTile(
                      title: Container(
                        decoration: BoxDecoration(
                          color: Colors.black87,
                          border: Border.all(),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Center(
                                child: Container(
                                  child: Text(
                                    title,
                                    textAlign: TextAlign.center,
                                    textScaleFactor: 3,
                                  ),
                                ),
                              ),
                              flex: 3,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                } else {
                  if (header == 'Invitations') {
                    return buildListItemInvitation(
                        context, listOfDocuments[index], currentUserId);
                  } else {
                    return buildListItemFriend(
                        context, listOfDocuments[index], currentUserId);
                  }
                }
              },
            ),
          ],
        )),
  );
}
