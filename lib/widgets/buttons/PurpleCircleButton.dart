import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PurpleCircleButton extends StatelessWidget {
  final Function onPressedFunction;
  final String text;

  PurpleCircleButton({this.onPressedFunction, this.text});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        this.onPressedFunction();
      },
      color: Colors.deepPurple,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          this.text,
          style: TextStyle(fontSize: 25, color: Colors.white),
        ),
      ),
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
    );
  }
}
