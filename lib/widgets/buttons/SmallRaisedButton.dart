
import 'package:flutter/material.dart';

class SmallRaisedButton extends StatelessWidget {
  final Function onPressedFunction;
  final String text;
  final Color color;

  SmallRaisedButton({this.onPressedFunction, this.text, this.color});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        this.onPressedFunction();
      },
      color: this.color,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          this.text,
          style: TextStyle(fontSize: 15, color: Colors.white),
        ),
      ),
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
    );
  }
}
