import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:uranos/models/User.dart';
import 'package:uranos/widgets/buttons/PurpleCircleButton.dart';

class UserInfoColumn extends StatelessWidget {
  final User user;
  final PurpleCircleButton button;

  UserInfoColumn({this.user, this.button});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        CircleAvatar(
          backgroundImage: NetworkImage(
            user.photoUrl,
          ),
          radius: 60,
          backgroundColor: Colors.transparent,
        ),
        SizedBox(height: 30),
        Text(
          'NAME',
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Text(
          user.displayName,
          style: TextStyle(
            fontSize: 25,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 20),
        Text(
          'EMAIL',
          style: TextStyle(
              fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        Text(
          user.email,
          style: TextStyle(
            fontSize: 25,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 40),
        button
      ],
    );
  }
}
