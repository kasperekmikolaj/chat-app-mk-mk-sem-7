import 'package:flutter/material.dart';

import 'screens/login_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login',
      theme: ThemeData(
        brightness: Brightness.dark,
        //primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}
