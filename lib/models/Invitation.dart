import 'User.dart';

class Invitation{
  User from;
  User to;
  bool accepted;
  DateTime data;

  Invitation(User from, User to){
    this.from = from;
    this.to = to;
    this.accepted = false;
    this.data = DateTime.now();
  }

  Map<String, dynamic> toJson() =>
      {
        'from': from.displayName,
        'to': to.displayName,
        'status': accepted,
        'data': data,
        'photoUrlFrom': from.photoUrl,
        'fromId': from.userId
      };
}