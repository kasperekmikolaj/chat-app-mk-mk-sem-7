class User {
  final String displayName;
  final String email;
  final String photoUrl;
  final String userId;

  User(this.userId, this.displayName, this.email, this.photoUrl);

  User.fromJsonSecureStorage(Map<String, dynamic> json)
      : userId = json['userId'],
        displayName = json['displayName'],
        email = json['email'],
        photoUrl = json['photoUrl'];

  User.fromJsonFirebase({Map<String, dynamic> json, userId: String})
      : userId = userId,
        displayName = json['displayName'],
        email = json['email'],
        photoUrl = json['photoUrl'];

  Map<String, dynamic> toJson() => {
        'userId': userId,
        'displayName': displayName,
        'email': email,
        'photoUrl': photoUrl,
      };
}
