import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'constants.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = GoogleSignIn();
final FlutterSecureStorage secureStorage = new FlutterSecureStorage();

String currentUserUID;

Future<void> signInWithGoogle() async {
  final GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
  final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;

  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  final AuthResult authResult = await _auth.signInWithCredential(credential);
  final FirebaseUser user = authResult.user;

  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);

  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);

  currentUserUID = currentUser.uid;

  String json = jsonEncode({
    'userId': currentUserUID,
    'displayName': user.displayName,
    'email': user.email,
    'photoUrl': user.photoUrl,
  });

  secureStorage.write(key: kCurrentUserKey, value: json);

  CollectionReference usersCollection = firestore.collection('users');
  DocumentSnapshot userData =
      await usersCollection.document('$currentUserUID').get();
  if (!userData.exists) {
    usersCollection.document('$currentUserUID').setData(
          ({
            'email': user.email,
            'displayName': user.displayName,
            'photoUrl': user.photoUrl
          }),
        );
  }
}

void signOutGoogle() async {
  await _googleSignIn.signOut();

  print("User Sign Out");
}
